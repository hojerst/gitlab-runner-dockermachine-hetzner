FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c AS downloads

# renovate: datasource=github-releases depName=JonasProgrammer/docker-machine-driver-hetzner
ENV DOCKER_MACHINE_DRIVER_HETZNER_VERSION=5.0.2

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

WORKDIR /work

RUN ARCH="$(uname -m | sed -e 's/^aarch64$/arm64/' -e 's/x86_64/amd64/')" \
 && v="$DOCKER_MACHINE_DRIVER_HETZNER_VERSION" \
 && wget -qO- "https://github.com/JonasProgrammer/docker-machine-driver-hetzner/releases/download/${v}/docker-machine-driver-hetzner_${v}_linux_${ARCH}.tar.gz" \
    | tar xz docker-machine-driver-hetzner

FROM registry.gitlab.com/gitlab-org/gitlab-runner:alpine-v16.8.0@sha256:6166ace6ddec56414ef27e4236dbbf8a7727803cf0cae98acb5badb27f3c7455

COPY --from=downloads /work/docker-machine-driver-hetzner /usr/bin/
