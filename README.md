# GitLab Runner with Hetzner Docker Machine Driver

This image that contains the latest version of the GitLab Runner and the Hetzner Docker Machine Driver.

Inspired by [hetzner-gitlab-runner](https://github.com/mawalu/hetzner-gitlab-runner), which is unfortunately no longer updated.

## Usage

Image is available at [hojerst/gitlab-runner-dockermachine-hetzner](https://hub.docker.com/r/hojerst/gitlab-runner-dockermachine-hetzner)

```yaml
# `gitlab/gitlab-runner` helm chart values:
image:
  registry: docker.io
  image: hojerst/gitlab-runner-dockermachine-hetzner
  tag: latest
```
